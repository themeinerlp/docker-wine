FROM node:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -yqq apt-transport-https
RUN dpkg --add-architecture i386
RUN wget -nc https://dl.winehq.org/wine-builds/winehq.key
RUN apt-key add winehq.key
RUN apt-add-repository https://dl.winehq.org/wine-builds/debian/
RUN apt-get update
RUN apt-get install --install-recommends winehq-stable -yqq
RUN export PATH=$PATH:/opt/wine-stable/bin
RUN wine --version 